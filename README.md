Torero-ci is a web service dedicated to integrate
with github (or gitlab) and provide testing of a ansible
module. 

(pun on the name dedicated to Robyn)

The problem it try to solve is the following:
- I have written a few roles (around 10 to 20) and
want to have a common way to test all of them. So far, I was
using travis, but this requires ugly cut and paste in each, along
with specific adaptation for tests that fail. 

So this service will centralize the tests, and I will just describe the 
one I want to skip in a file.

(also, i wanted a excuse to learn golang)

TODO:
- write a service that implement github webhooks
- write a service that implement gitlab webhooks, if different
- write code that run some tests:
 - run ansible-lint in docker
 - run ansible-check in docker
 - run flake8 on python code on files/
 - run bash linter on bash code on files/
 - investigate other 
- integrate with a kubernetes cluster (ie, run jobs)
